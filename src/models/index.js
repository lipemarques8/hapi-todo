const path = require('path');
const fs = require('fs');
const _ = require('underscore');
const mixins = require('src/util/mixins');

fs.readdirSync(__dirname).forEach(function (file) {
  /* If its the current file ignore it */
  if (file === 'index.js') return;

  /* Prepare empty object to store module */
  let mod = {};

  let capitalized = mixins.capitalize(file);

  /* Store module with its name (from filename) */
  mod[path.basename(capitalized, '.js')] = require(path.join(__dirname, file));

  /* Extend module.exports (in this case - undescore.js, can be any other) */
  _.extend(module.exports, mod);
});
