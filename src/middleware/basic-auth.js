"use strict";

const UserModel = require('src/models/user');
const userDAO = require('src/dao/user');
const Joi = require('joi');

module.exports = function(username, password, callback) {

	let userModel = new UserModel();
	let cryptedPassword = userModel.encryptPass(password);

	userDAO.find(username, cryptedPassword, function(err, credentials) {
		if (err) return callback(err, false);

		let isValid = validate(credentials[0]);

		callback(null, isValid, credentials[0]);
	});
};

function validate(credentials) {
	credentials = credentials || {};
	let schema = {
		userId: Joi.number().integer().required(),
		email: Joi.string().required()
	};

	let err = Joi.validate(credentials, schema, {allowUnknown:true});
	return err === null;
}
