"use strict";

const db = require('src/middleware/db');

function UserDAO(){};
UserDAO.prototype = (function(){

	return {
		find: function find(email, password, callback) {
			let values = [
				email,
				password
			];

			let sql = 'SELECT userId, email, pass FROM user AS u '+
				'WHERE u.email = ? ' +
				'AND u.pass = ?';

			db.query({
				sql : sql,
				values: values,
				callback : callback
			});
		}
	};
})();

const userDAO = new UserDAO();
module.exports = userDAO;
