"use strict";

const db = require('src/middleware/db');

function TaskDAO(){};
TaskDAO.prototype = (function(){

	return {
		findByID: function findByID(params, callback) {

			let values = [
				params.taskId,
				params.userId
			];

			let sql = "select taskId, description from task"+
					" where taskId = ?"+
					" and userId = ?";

			db.query({
				sql : sql,
				values: values,
				callback : callback
			});
		},
		find: function find(params, callback) {

			let values = [
				params.userId
			];

			let sql = "select userId, taskId, description from task"+
					" where userId = ?";

			db.query({
				sql : sql,
				values: values,
				callback : callback
			});
		},
		insert: function insert (params, callback) {

			let values = [
				params.userId,
				params.description
			];

			let sql = "insert into task "+
					" (userId, description)"+
					" values (?,?)";

			db.query({
				sql : sql,
				values: values,
				callback : callback
			});
		},
		update: function update (params, callback) {

			let values = [
				params.description,
				params.userId,
				params.taskId
			];

			let sql = "update task "+
					" set description = ? "+
					" where userId = ? "+
					" and taskId = ? ";

			db.query({
				sql : sql,
				values: values,
				callback : callback
			});
		},
		delete: function (params, callback) {

			let values = [
				params.taskId,
				params.userId
			];

			let sql = "delete from task"+
					" where taskId = ?"+
					" and userId = ?";

			db.query({
				sql : sql,
				values: values,
				callback : callback
			});
		},
	};
})();

const taskDAO = new TaskDAO();
module.exports = taskDAO;
