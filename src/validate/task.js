"use strict";

const _ = require('underscore');
const Joi = require('joi');

const models = require('src/models');

function TaskValidate(){};
TaskValidate.prototype = (function(){

	return {
		findByID: {
			path: (function path() {
				let taskSchema = new models.Task().schema;
				return {
					task_id : taskSchema.taskId.required().rename('taskId')
				};
			})()
		},
		find : {
			query: (function query() {
				let taskSchema = new models.Task().schema;
				return {
					description : taskSchema.description
				};
			})()
		},
		insert: {
			payload: (function payload() {
				let taskSchema = new models.Task().schema;
				return {
					description : taskSchema.description.required()
				};
			})()
		},
		update: (function update() {
			let taskSchema = new models.Task().schema;
			return {
				path: {
					task_id : taskSchema.taskId.required().rename('taskId')
				},
				payload: {
					description : taskSchema.description.required()
				}
			}
		})(),
		delete: {
			path: (function path() {
				let taskSchema = new models.Task().schema;
				return {
					task_id : taskSchema.taskId.required().rename('taskId')
				};
			})()
		}
	};
})();

const taskValidate = new TaskValidate();
module.exports = taskValidate;
