"use strict";

const Hapi = require('hapi');
const Q = require('q');
const taskDAO = require('src/dao/task');
const _ = require('underscore');

const ReplyHelper = require('src/controllers/reply-helper');

function TaskController(){};
TaskController.prototype = (function(){

	return {
		findByID: function findByID(request, reply) {

			let helper = new ReplyHelper(request, reply);
			let params = request.plugins.createControllerParams(request.params);

			taskDAO.findByID(params, function (err, data) {
				helper.replyFindOne(err, data);
			});
		},
		find: function find(request, reply) {

			let helper = new ReplyHelper(request, reply);
			let params = request.plugins.createControllerParams(request.query);

			taskDAO.find(params, function (err, data) {
				helper.replyFind(err, data);
			});
		},
		insert: function insert(request, reply) {

			let helper = new ReplyHelper(request, reply);
			let params = request.plugins.createControllerParams(request.payload);

			let insert = Q.denodeify(taskDAO.insert);
			let findByID = Q.denodeify(taskDAO.findByID);

			insert(params).then(function insert(data) {

				let result = data;
				if (result.exception) {
					reply(Hapi.error.badRequest(result.exception));
					done();
				}
				params.taskId = result.insertId;
				return findByID(params);

			}).then(function (data) {

				let location = helper.url + request.path + '/' + params.taskId;

				reply(data[0])
					.type('application/json')
					.code(201)
					.header('Location', location);

			}).catch(function (err) {
				reply(Hapi.error.badImplementation(err));
			});
		},
		update: function update(request, reply) {

			let helper = new ReplyHelper(request, reply);
			let payload = request.plugins.createControllerParams(request.payload);
			let params = request.plugins.createControllerParams(request.params);

			_.extend(params, payload);

			let update = Q.denodeify(taskDAO.update);
			let findByID = Q.denodeify(taskDAO.findByID);

			update(params).then(function update(data) {

				let result = data;
				if (result.exception) {
					reply(Hapi.error.badRequest(result.exception));
					done();
				}
				return findByID(params);

			}).then(function (data) {

				reply(data[0])
					.type('application/json');

			}).catch(function (err) {
				reply(Hapi.error.badImplementation(err));
			});

		},
		delete: function (request, reply){

			let helper = new ReplyHelper(request, reply);
			let params = request.plugins.createControllerParams(request.params);

			taskDAO.delete(params, function (err, data) {
				helper.replyDelete(err, data);
			});
		}
	}
})();

const taskController = new TaskController();
module.exports = taskController;
