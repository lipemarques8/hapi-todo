"use strict";

const Hapi = require('hapi');
const constants = require('src/config/constants.js');
const basicAuth = require('src/middleware/basic-auth');
const routes = require('src/routes');
const _ = require('underscore');

const options = {
	state : {
		cookies : {
			strictHeader : false
		}
	}
};

const host = constants.application['host'];
const port = constants.application['port'];
const server = Hapi.createServer(host, port, options);

server.pack.require('hapi-auth-basic', function (err) {
	server.auth.strategy('simple', 'basic', true, {
		validateFunc: basicAuth
	});
});

server.ext('onRequest', function(request, next){
	request.plugins.createControllerParams = function(requestParams){
		const params = _.clone(requestParams);
		params.userId = request.auth.credentials.userId;
		return params;
	};
	next();
});

// Add all the routes within the routes folder
for (const route in routes) {
	server.route(routes[route]);
}

module.exports = server;

if (process.env.NODE_ENV !== 'test') {
	server.start();

	console.log('Server running in port #'+port);
}
